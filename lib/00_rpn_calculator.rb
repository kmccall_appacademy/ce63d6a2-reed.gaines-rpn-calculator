class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def tokens(str)
    arr = str.split
    symbols = "*/+-"
    arr.map do |el|
      if symbols.include?(el)
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def value
    @stack[-1]
  end

  def plus
    if @stack[-1] == nil
      raise "calculator is empty"
    end
    result = @stack.pop + @stack.pop
    @stack << result
  end

  def minus
    if @stack[-1] == nil
      raise "calculator is empty"
    end
    num_2 = @stack.pop
    num_1 = @stack.pop
    result = num_1 - num_2
    @stack << result
  end

  def times
    if @stack[-1] == nil
      raise "calculator is empty"
    end
    result = @stack.pop * @stack.pop
    @stack << result
  end

  def divide
    if @stack[-1] == nil
      raise "calculator is empty"
    end
    num_2 = @stack.pop.to_f
    num_1 = @stack.pop.to_f
    result = num_1 / num_2
    @stack << result
  end

  def evaluate(str)
    tokenized = tokens(str)
    tokenized.each do |t|
      if t.class == Fixnum
        push(t)
      elsif t == :+
        self.plus
      elsif t == :-
        self.minus
      elsif t == :*
        self.times
      elsif t == :/
        self.divide
      end
    end
    value
  end

end
